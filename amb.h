#ifndef AMB_H
#define AMB_H

// Returns an integer in the range [0, n) which will cause the program to
// exit successfully.  If no such integer exists, exits unsuccessfully.
int amb(int n);

// Same as `amb`, but chooses an integer which causes an unsuccessful exit.
// If no such integer exists, exits successfully.
int smoke(int n);

// Same as `amb`, but ignores exit code and always exits successfully,
// after running the remainder of the program once for all n return values. 
// `nondet(1)` can be used to override the eventual exit with a successful one.
int nondet(int n);

// Invert the eventual exit code.  If success is inverted to failure,
// the given status code is used.
void invert_exit(int status);

// When first called, this function returns zero.  If the program exits
// unsuccessfully, it will restart from this function call, which will
// return the unsuccessful exit code.
int groundhog_day(void);

#endif
