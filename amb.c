#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "amb.h"

int amb(const int n)
{
  if (n == 0) _exit(42);

  for (int i = 0; i < n - 1; ++i)
  {
    const pid_t child = fork();
    if (child < 0)
    {
      perror("fork");
      abort();
    }

    if (child == 0)
      return i;

    int wstatus;
    if (waitpid(child, &wstatus, 0) < 0)
    {
      perror("wait");
      abort();
    }

    if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == 0)
      _exit(0);
  }

  return n - 1;
}


int smoke(const int n)
{
  if (n == 0) _exit(0);

  for (int i = 0; i < n - 1; ++i)
  {
    const pid_t child = fork();
    if (child < 0)
    {
      perror("fork");
      abort();
    }

    if (child == 0)
      return i;

    int wstatus;
    if (waitpid(child, &wstatus, 0) < 0)
    {
      perror("wait");
      abort();
    }

    if (WIFSIGNALED(wstatus))
      _exit(128 + WTERMSIG(wstatus));

    if (WEXITSTATUS(wstatus) != 0)
      _exit(WEXITSTATUS(wstatus));
  }

  return n - 1;
}


int nondet(const int n)
{
  for (int i = 0; i < n; ++i)
  {
    const pid_t child = fork();
    if (child < 0)
    {
      perror("fork");
      abort();
    }

    if (child == 0)
      return i;

    if (waitpid(child, NULL, 0) < 0)
    {
      perror("wait");
      abort();
    }
  }

  _exit(0);
}


void invert_exit(const int status)
{
  const pid_t child = fork();
  if (child < 0)
  {
    perror("fork");
    abort();
  }

  if (child == 0)
    return;

  int wstatus;
  if (waitpid(child, &wstatus, 0) < 0)
  {
    perror("wait");
    abort();
  }

  if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == 0)
    _exit(status);

  _exit(0);
}


int groundhog_day(void)
{
  int last_status = 0;

  do
  {
    const pid_t child = fork();
    if (child < 0)
    {
      perror("fork");
      abort();
    }

    if (child == 0)
      return last_status;

    int wstatus;
    if (waitpid(child, &wstatus, 0) < 0)
    {
      perror("wait");
      abort();
    }

    if (WIFEXITED(wstatus))
      last_status = WEXITSTATUS(wstatus);
    else
      last_status = 128 + WTERMSIG(wstatus);
  } while (last_status != 0);

  _exit(0);
}
